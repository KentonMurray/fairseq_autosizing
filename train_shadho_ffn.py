import shadho_worker
import train
from fairseq import options

import json
import sys

def train_shadho_ffn(params):
    print("params:", params)

    #args_str = "-a transformer "
    #args_str = ["--arch", "transformer_shadho", "--ddp-backend", "c10d"] #"no_c10d"]
    args_str = ["--arch", "transformer_shadho_ffn", "--ddp-backend", "no_c10d"]
    #json_params = json.loads(params) #TODO: single vs double quotes
    #json_params = params[2]
    json_params = params
    d_model = json_params["d_model"]
    N = json_params["N"]

    # Layers and Layer Sizes
    N_encoder = json_params["N_encoder"]
    N_decoder = json_params["N_decoder"]
    dumb_encoder = json_params["dumb_encoder"]
    dumb_decoder = json_params["dumb_decoder"]
    dumb_encoder_str = ""
    dumb_encoder_counter = 0
    for key in dumb_encoder:
        ffn_encoder = dumb_encoder[key]
        dumb_encoder_str = str(ffn_encoder) + "," + dumb_encoder_str
        dumb_encoder_counter = dumb_encoder_counter + 1
        if dumb_encoder_counter >= int(N_encoder):
            dumb_encoder_str = dumb_encoder_str.rstrip(",")
            break
    dumb_decoder_str = ""
    dumb_decoder_counter = 0
    for key in dumb_decoder:
        ffn_decoder = dumb_decoder[key]
        dumb_decoder_str = str(ffn_decoder) + "," + dumb_decoder_str
        dumb_decoder_counter = dumb_decoder_counter + 1
        if dumb_decoder_counter >= int(N_decoder):
            dumb_decoder_str = dumb_decoder_str.rstrip(",")
            break
    print("dumb_encoder_str:", dumb_encoder_str)
    print("dumb_decoder_str:", dumb_decoder_str)

    optimizer_obj = json_params["optimizer"]
    for key in optimizer_obj:
        optimizer = key
        optimizer_values = optimizer_obj[optimizer]
        print("KEY:", key)
        print(optimizer_values)
        lr = optimizer_values["lr"]
        beta_1 = optimizer_values["beta_1"]
        beta_2 = optimizer_values["beta_2"]
        adam_betas = "(" + str(beta_1) + ", " + str(beta_2) + ")"
        print("adam_betas:", adam_betas)
        weight_decay = optimizer_values["weight_decay"]
        args_str.append("--lr")
        #args_str.append(str(lr))
        args_str.append("0.0001")   #TODO: NOT HARDCODED
        args_str.append("--weight-decay")
        #args_str.append(str(weight_decay))
        args_str.append("0.0") #TODO: NOT HARDCODED
    layers = json_params["layers"]
    print("d_model:", d_model)
    print("N:", N)
    print("optimizer:", optimizer)
    print("layers:", layers)
    args_str.append("--optimizer")
    args_str.append(optimizer)
    for key in json_params:
         print("KENTON:", key, json_params[key])
    layers = json_params["layers"]
    encoder = layers["encoder"]
    decoder = layers["decoder"]
    print("     encoder:", encoder)
    print("     decoder:", decoder)

    #print(encoder["multi_head_attention"]["h"])
    encoder_attention_heads = encoder["multi_head_attention"]["h"]
    #print(encoder["feedforward"]["neurons"])
    encoder_ffn_embed_dim = encoder["feedforward"]["neurons"]

    print(decoder["multi_head_attention_1"]["h"])
    decoder_attention_heads = decoder["multi_head_attention_1"]["h"]
    print(decoder["feedforward"]["neurons"])
    decoder_ffn_embed_dim = decoder["feedforward"]["neurons"]

    args_str2 = []
    args_str2.append('--encoder-ffn-embed-dim')
    args_str2.append(str(encoder_ffn_embed_dim))
    args_str2.append('--encoder-ffn-embed-dim-multiple')
    args_str2.append(dumb_encoder_str)
    args_str2.append('--encoder-layers')
    args_str2.append(str(N_encoder))
    args_str2.append('--decoder-ffn-embed-dim')
    args_str2.append(str(decoder_ffn_embed_dim))
    args_str2.append('--decoder-ffn-embed-dim-multiple')
    args_str2.append(dumb_decoder_str)
    args_str2.append('--decoder-layers')
    args_str2.append(str(N_decoder))

    #args_str2.append(' '.join(sys.argv[1:]))
    args_strlinf1 = args_str2
    args_str2.extend(sys.argv[1:])
    print("sys.argv[1:]", sys.argv)

    args_str3 = args_str + args_str2
    print("args_str:", args_str)
    print("args_str2:", args_str2)
    print("args_str3:", args_str3)



    # Normal argparse
    parser = options.get_shadho_parser()
    print(parser)
    parser.parse_args(args_str)
    args = options.parse_args_and_arch(parser, args_str3)
    print("ARGS", args)

    train.main(args)



    return 0.001

if __name__ == '__main__':
    import json
    shadho_worker.run(train_shadho_ffn)

