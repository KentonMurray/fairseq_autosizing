import shadho_worker
import train
from fairseq import options

import json
import sys

def train_shadho(params):
    print("params:", params)

    args_str = ["--arch", "transformer_shadho", "--ddp-backend", "no_c10d"]
    #json_params = json.loads(params) #TODO: single vs double quotes
    json_params = params
    d_model = json_params["d_model"]
    N = json_params["N"]
    N_encoder = json_params["N_encoder"]
    N_decoder = json_params["N_decoder"]
    dumb_encoder = json_params["dumb_encoder"]
    dumb_decoder = json_params["dumb_decoder"]
    dumb_encoder_str = ""
    dumb_encoder_counter = 0
    for key in dumb_encoder:
        ffn_encoder = dumb_encoder[key]
        dumb_encoder_str = str(ffn_encoder) + "," + dumb_encoder_str
        dumb_encoder_counter = dumb_encoder_counter + 1
        if dumb_encoder_counter >= int(N_encoder):
            dumb_encoder_str = dumb_encoder_str.rstrip(",")
            break
    dumb_decoder_str = ""
    dumb_decoder_counter = 0
    for key in dumb_decoder:
        ffn_decoder = dumb_decoder[key]
        dumb_decoder_str = str(ffn_decoder) + "," + dumb_decoder_str
        dumb_decoder_counter = dumb_decoder_counter + 1
        if dumb_decoder_counter >= int(N_decoder):
            dumb_decoder_str = dumb_decoder_str.rstrip(",")
            break
    print("dumb_encoder_str:", dumb_encoder_str)
    print("dumb_decoder_str:", dumb_decoder_str)
    optimizer_obj = json_params["optimizer"]
    for key in optimizer_obj:
        optimizer = key
        optimizer_values = optimizer_obj[optimizer]
        print("KEY:", key)
        print(optimizer_values)
        lr = optimizer_values["lr"]
        beta_1 = optimizer_values["beta_1"]
        beta_2 = optimizer_values["beta_2"]
        adam_betas = "(" + str(beta_1) + ", " + str(beta_2) + ")"
        print("adam_betas:", adam_betas)
        weight_decay = optimizer_values["weight_decay"]
        args_str.append("--lr")
        args_str.append(str(lr))
        args_str.append("--weight-decay")
        args_str.append(str(weight_decay))
        #for item in optimizer_values:
        #    print(item, optimizer_values[item])
        #    args_str.append("--" + str(item))
        #    args_str.append(str(optimizer_values[item]))
    layers = json_params["layers"]
    print("d_model:", d_model)
    print("N:", N)
    print("optimizer:", optimizer)
    print("layers:", layers)
    args_str.append("--optimizer")
    args_str.append(optimizer)
    for key in json_params:
         print("KENTON:", key, json_params[key])
    layers = json_params["layers"]
    encoder = layers["encoder"]
    decoder = layers["decoder"]
    print("     encoder:", encoder)
    print("     decoder:", decoder)

    #print(encoder["multi_head_attention"]["h"])
    encoder_attention_heads = encoder["multi_head_attention"]["h"]
    #print(encoder["multi_head_attention"]["scaled_dot_product"]["activation"])
    activation = encoder["multi_head_attention"]["scaled_dot_product"]["activation"]
    #print(encoder["multi_head_attention"]["scaled_dot_product"]["dropout"])
    dropout = encoder["multi_head_attention"]["scaled_dot_product"]["dropout"]
    #print(encoder["feedforward"]["neurons"])
    encoder_ffn_embed_dim = encoder["feedforward"]["neurons"]

    print(decoder["multi_head_attention_1"]["h"])
    decoder_attention_heads = decoder["multi_head_attention_1"]["h"]
    print(decoder["multi_head_attention_1"]["scaled_dot_product"]["activation"])
    activation = decoder["multi_head_attention_1"]["scaled_dot_product"]["activation"]
    print(decoder["feedforward"]["neurons"])
    decoder_ffn_embed_dim = decoder["feedforward"]["neurons"]

    args_str2 = []
    args_str2.append('--encoder-embed-dim')
    args_str2.append(str(d_model))
    args_str2.append('--encoder-ffn-embed-dim')
    args_str2.append(str(encoder_ffn_embed_dim))
    args_str2.append('--encoder-attention-heads')
    args_str2.append(str(encoder_attention_heads))
    args_str2.append('--encoder-layers')
    args_str2.append(str(N))
    args_str2.append('--decoder-embed-dim')
    args_str2.append(str(d_model))
    args_str2.append('--decoder-ffn-embed-dim')
    args_str2.append(str(decoder_ffn_embed_dim))
    args_str2.append('--decoder-attention-heads')
    args_str2.append(str(decoder_attention_heads))
    args_str2.append('--decoder-layers')
    args_str2.append(str(N))
    args_str2.append('--dropout')
    args_str2.append(str(dropout))
    args_str2.append("--adam-betas")
    args_str2.append(str(adam_betas))

    args_strl21 = args_str2
    args_strlinf1 = args_str2
    args_str2.extend(sys.argv[1:])
    print("sys.argv[1:]", sys.argv)

    args_str3 = args_str + args_str2
    print("args_str:", args_str)
    print("args_str2:", args_str2)
    print("args_str3:", args_str3)

    #L21
    savedirl21 = sys.argv[5]+"-l21"
    savedirlinf1 = sys.argv[5]+"-linf1"
    sys.argv[5] = savedirl21
    print(sys.argv)
    args_str3_l21 = args_str3.copy()
    args_str3_l21.append("--l21")
    args_str3_l21.append("0.25")
    print("BEFORE:", args_str3_l21)
    print("34:", args_str3_l21[34])
    args_str3_l21[34] = savedirl21
    print("AFTER:", args_str3_l21)

    #LINF1
    args_str3_linf1 = args_str3.copy()
    args_str3_linf1.append("--linf1")
    args_str3_linf1.append("0.5")
    print("BEFORE:", args_str3_linf1)
    print("34:", args_str3_linf1[34])
    args_str3_linf1[34] = savedirlinf1
    print("AFTER:", args_str3_linf1)


    print("args_str3_l21:", args_str3_l21)
    print("args_str3_inf1:", args_str3_linf1)

    # Normal argparse
    parser = options.get_shadho_parser()
    print(parser)
    parser.parse_args(args_str) #TODO: Make sure these do not get overwritten
    args = options.parse_args_and_arch(parser, args_str3) #TODO: Figure out what the second parse does...
    print("ARGS", args)

    train.main(args)


    return 0.001

if __name__ == '__main__':
    import json
    shadho_worker.run(train_shadho)
